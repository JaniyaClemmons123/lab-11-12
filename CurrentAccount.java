public class CurrentAccount extends Account {
    public CurrentAccount(double balance, String name) {
        //call to super class constructor 
        super(balance,name); 
    }

    @Override 
    public void addInterest(){
         super.setBalance(super.getBalance()*1.4);
    }
}