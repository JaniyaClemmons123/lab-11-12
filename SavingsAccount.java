public class SavingsAccount extends Account {
    
    public SavingsAccount(double balance, String name) {
        //call to super class constructor 
        super(balance,name); 
    }

    @Override 
    public void addInterest(){
         super.setBalance(super.getBalance()*1.1);
    }
}