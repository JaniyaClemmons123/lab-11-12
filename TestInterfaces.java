public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] dets = new Detailable[3];
        SavingsAccount acc1 = new SavingsAccount(2.0, "Jay");
        HomeInsurance home = new HomeInsurance(4.0, 3.0, 2.0);
        CurrentAccount cur = new CurrentAccount(6.0, "Jay");
        //Polymorphism allows you to place different types of accounts into account array
        dets[0] = acc1;
        dets[1] = home;
        dets[2] = cur; 
        
        for(int i = 0; i < dets.length; i++ ){
            System.out.println(dets[i].getDetails());
            
        }
    }
}